CONTRACTS = contrats
TEMPLATE  = modeles

CP    = cp -r

all: build

new_employee:
	@echo Nom employé ?
	@read line; echo $$line | xargs -i $(CP) '$(TEMPLATE)/employe.tex' '$(CONTRACTS)/Employee-{}.tex'

new_layoff:
	@echo Nom employé ?
	@read line; echo $$line | xargs -i $(CP) '$(TEMPLATE)/licenciement.tex' '$(CONTRACTS)/Layoff-{}.tex'

new_company:
	@echo Nom entreprise ?
	@read line; echo $$line | xargs -i $(CP) '$(TEMPLATE)/entreprise.tex' '$(CONTRACTS)/Company-{}.tex'
